(function() { // the self-executing anonymous function
  var command = require("josm/command");
  var console = require("josm/scriptingconsole");
  // Displays an error message box with the given message.
  function throwErr(errorMessage){
    josm.alert(errorMessage, {
       title: "Error Alert",
       messageType: "error"
    });
    throw "error"; // This stops the script.
  }

  // Convert the input string to lower case and
  // Capitalize the first letter of each word.
  function capitalize(inputString){
    inputString = inputString.toString().toLowerCase();
    capString = "";

    var words = inputString.split(" ");
    words.forEach(function(word){
      var firstLetter = word.substr(0,1).toUpperCase();
      var capWord = firstLetter.concat(word.substr(1)); // Replace first letter with captialization
      capString = capString.concat(" ").concat(capWord);
    });
    return capString.trim(); // Remove leading space
  }

  // Converts direction abbreviations to direction names.
  const dirMap = {"N":"NORTH",
      "S":"SOUTH",
      "E":"EAST",
      "W":"WEST"};
  function getDirName(dirAbbr){
    var dirName = dirMap[dirAbbr];
    if(dirName == undefined) throwErr("Direction mapping not found: \""+dirAbbr+"\"");
    return capitalize(dirName);
  }

  // Converts street type abbreviations to street type names.
  // This is the USPS Street suffix abbreviation map, created from here: https://pe.usps.com/text/pub28/28apc_002.htm
  const streetTypeMap = {"ALY":"ALLEY",
    "ANX":"ANEX",
    "ARC":"ARCADE",
    "AVE":"AVENUE",
    "BYU":"BAYOU",
    "BCH":"BEACH",
    "BND":"BEND",
    "BLF":"BLUFF",
    "BLFS":"BLUFFS",
    "BTM":"BOTTOM",
    "BLVD":"BOULEVARD",
    "BR":"BRANCH",
    "BRG":"BRIDGE",
    "BRK":"BROOK",
    "BRKS":"BROOKS",
    "BG":"BURG",
    "BGS":"BURGS",
    "BYP":"BYPASS",
    "CP":"CAMP",
    "CYN":"CANYON",
    "CPE":"CAPE",
    "CSWY":"CAUSEWAY",
    "CTR":"CENTER",
    "CTRS":"CENTERS",
    "CIR":"CIRCLE",
    "CIRS":"CIRCLES",
    "CLF":"CLIFF",
    "CLFS":"CLIFFS",
    "CLB":"CLUB",
    "CMN":"COMMON",
    "CMNS":"COMMONS",
    "COR":"CORNER",
    "CORS":"CORNERS",
    "CRSE":"COURSE",
    "CT":"COURT",
    "CTS":"COURTS",
    "CV":"COVE",
    "CVS":"COVES",
    "CRK":"CREEK",
    "CRES":"CRESCENT",
    "CRST":"CREST",
    "XING":"CROSSING",
    "XRD":"CROSSROAD",
    "XRDS":"CROSSROADS",
    "CURV":"CURVE",
    "DL":"DALE",
    "DM":"DAM",
    "DV":"DIVIDE",
    "DR":"DRIVE",
    "DRS":"DRIVES",
    "EST":"ESTATE",
    "ESTS":"ESTATES",
    "EXPY":"EXPRESSWAY",
    "EXT":"EXTENSION",
    "EXTS":"EXTENSIONS",
    "FALL":"FALL",
    "FLS":"FALLS",
    "FRY":"FERRY",
    "FLD":"FIELD",
    "FLDS":"FIELDS",
    "FLT":"FLAT",
    "FLTS":"FLATS",
    "FRD":"FORD",
    "FRDS":"FORDS",
    "FRST":"FOREST",
    "FRG":"FORGE",
    "FRGS":"FORGES",
    "FRK":"FORK",
    "FRKS":"FORKS",
    "FT":"FORT",
    "FWY":"FREEWAY",
    "GDN":"GARDEN",
    "GDNS":"GARDENS",
    "GTWY":"GATEWAY",
    "GLN":"GLEN",
    "GLNS":"GLENS",
    "GRN":"GREEN",
    "GRNS":"GREENS",
    "GRV":"GROVE",
    "GRVS":"GROVES",
    "HBR":"HARBOR",
    "HBRS":"HARBORS",
    "HVN":"HAVEN",
    "HTS":"HEIGHTS",
    "HWY":"HIGHWAY",
    "HL":"HILL",
    "HLS":"HILLS",
    "HOLW":"HOLLOW",
    "INLT":"INLET",
    "IS":"ISLAND",
    "ISS":"ISLANDS",
    "ISLE":"ISLE",
    "JCT":"JUNCTION",
    "JCTS":"JUNCTIONS",
    "KY":"KEY",
    "KYS":"KEYS",
    "KNL":"KNOLL",
    "KNLS":"KNOLLS",
    "LK":"LAKE",
    "LKS":"LAKES",
    "LAND":"LAND",
    "LNDG":"LANDING",
    "LN":"LANE",
    "LGT":"LIGHT",
    "LGTS":"LIGHTS",
    "LF":"LOAF",
    "LCK":"LOCK",
    "LCKS":"LOCKS",
    "LDG":"LODGE",
    "LOOP":"LOOP",
    "MALL":"MALL",
    "MNR":"MANOR",
    "MNRS":"MANORS",
    "MDW":"MEADOW",
    "MDWS":"MEADOWS",
    "MEWS":"MEWS",
    "ML":"MILL",
    "MLS":"MILLS",
    "MSN":"MISSION",
    "MTWY":"MOTORWAY",
    "MT":"MOUNT",
    "MTN":"MOUNTAIN",
    "MTNS":"MOUNTAINS",
    "NCK":"NECK",
    "ORCH":"ORCHARD",
    "OVAL":"OVAL",
    "OPAS":"OVERPASS",
    "PARK":"PARK",
    "PARK":"PARKS",
    "PKWY":"PARKWAY",
    "PKWY":"PARKWAYS",
    "PASS":"PASS",
    "PSGE":"PASSAGE",
    "PATH":"PATH",
    "PIKE":"PIKE",
    "PNE":"PINE",
    "PNES":"PINES",
    "PL":"PLACE",
    "PLN":"PLAIN",
    "PLNS":"PLAINS",
    "PLZ":"PLAZA",
    "PT":"POINT",
    "PTS":"POINTS",
    "PRT":"PORT",
    "PRTS":"PORTS",
    "PR":"PRAIRIE",
    "RADL":"RADIAL",
    "RAMP":"RAMP",
    "RNCH":"RANCH",
    "RPD":"RAPID",
    "RPDS":"RAPIDS",
    "RST":"REST",
    "RDG":"RIDGE",
    "RDGS":"RIDGES",
    "RIV":"RIVER",
    "RD":"ROAD",
    "RDS":"ROADS",
    "RTE":"ROUTE",
    "ROW":"ROW",
    "RUE":"RUE",
    "RUN":"RUN",
    "SHL":"SHOAL",
    "SHLS":"SHOALS",
    "SHR":"SHORE",
    "SHRS":"SHORES",
    "SKWY":"SKYWAY",
    "SPG":"SPRING",
    "SPGS":"SPRINGS",
    "SPUR":"SPUR",
    "SPUR":"SPURS",
    "SQ":"SQUARE",
    "SQS":"SQUARES",
    "STA":"STATION",
    "STRA":"STRAVENUE",
    "STRM":"STREAM",
    "ST":"STREET",
    "STS":"STREETS",
    "SMT":"SUMMIT",
    "TER":"TERRACE",
    "TRWY":"THROUGHWAY",
    "TRCE":"TRACE",
    "TRAK":"TRACK",
    "TRFY":"TRAFFICWAY",
    "TRL":"TRAIL",
    "TRLR":"TRAILER",
    "TUNL":"TUNNEL",
    "TPKE":"TURNPIKE",
    "UPAS":"UNDERPASS",
    "UN":"UNION",
    "UNS":"UNIONS",
    "VLY":"VALLEY",
    "VLYS":"VALLEYS",
    "VIA":"VIADUCT",
    "VW":"VIEW",
    "VWS":"VIEWS",
    "VLG":"VILLAGE",
    "VLGS":"VILLAGES",
    "VL":"VILLE",
    "VIS":"VISTA",
    "WALK":"WALK",
    "WALK":"WALKS",
    "WALL":"WALL",
    "WAY":"WAY",
    "WAYS":"WAYS",
    "WL":"WELL",
    "WLS":"WELLS"};
  function getStreetTypeName(streetTypeAbbr){
    var streetTypeName = streetTypeMap[streetTypeAbbr];
    if(streetTypeName == undefined) throwErr("StreetType mapping not found: \""+streetTypeAbbr+"\"");
    return capitalize(streetTypeName);
  }

  // Returns true if the two objects have the same address tagging.
  function addrEquals(obj1, obj2){
    var equals = obj1.get("addr:housenumber") == obj2.get("addr:housenumber") &&
        obj1.get("addr:street") == obj2.get("addr:street") &&
        obj1.get("addr:city") == obj2.get("addr:city") &&
        obj1.get("addr:state") == obj2.get("addr:state") &&
        obj1.get("addr:postcode") == obj2.get("addr:postcode");
    return equals;
  }

  // Process a single address point.
  function processAddressPoint(obj){
    // Convert tags to OSM tags.
    if(obj.has("AddNum")) obj.set("addr:housenumber",obj.get("AddNum"));
    if(obj.has("ZipCode")) obj.set("addr:postcode",obj.get("ZipCode"));
    if(obj.has("State")) obj.set("addr:state",obj.get("State"));
    if(obj.has("City")) obj.set("addr:city",capitalize(obj.get("City")));

    if(obj.has("LandmarkNa")){
      var landmarkNa = obj.get("LandmarkNa");
      if(landmarkNa != "" && landmarkNa.toLowerCase() != "unknown")
        obj.set("name",capitalize(landmarkNa));
    }

    if(obj.has("StreetName")) {
      var streetName = capitalize(obj.get("StreetName"));
      if(obj.has("SuffixDir")){ // In this case, it's a numbered street like 900 South
        var dirName = getDirName(obj.get("SuffixDir"));
        streetName = streetName.concat(" ").concat(dirName);
      }
      else if(obj.has("StreetType")){ // In this case, it's a named street like Abc Avenue
        var streetTypeName = getStreetTypeName(obj.get("StreetType"));
        streetName = streetName.concat(" ").concat(streetTypeName);
      }
      obj.set("addr:street",streetName);
    }
  }




  // Processor starts here.
  var layer = josm.layers.activeLayer; // Set to the active layer.
  var ds = layer.data;

  // Convert tagging.
  var counter = 0;
  ds.each(function(obj){
    if(obj.isNode){ // Only adjust tagging for node shapes.
      processAddressPoint(obj);
      counter = counter + 1;
    }
  });
  josm.alert("Converted AGRC Address tagging on "+counter+" objects.");

  // Remove duplicate address points.
  var checkedObjs = [,];
  var removeObjs = [,];
  ds.each(function(obj){
    if(obj.isNode){
      if(obj.get("PtType")=="BASE ADDRESS") removeObjs.push(obj); // Automatically remove base address points.
      else {
        var match = checkedObjs.find(function(checkedObj){
          return addrEquals(obj, checkedObj);
        });
        if(match == undefined) checkedObjs.push(obj);
        else removeObjs.push(obj);
      }
    }
  });
  removeObjs.forEach(function(obj){
    ds.remove(obj.id, "node"); // Assume it's a node.
  });
  josm.alert("Removed "+removeObjs.length+" duplicate addresses.");
})();
