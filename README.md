# README #

### What is this repository for? ###

This houses scripts used to automate the data transformation in the [OpenStreetMap UtahBuildingsImport](https://wiki.openstreetmap.org/wiki/Utah/UtahBuildingsImport)

### How do I use these scripts? ###

* Read the [UtahBuildingsImport Wiki Page](https://wiki.openstreetmap.org/wiki/Utah/UtahBuildingsImport)
* Be sure you have the following installed:
    * [JOSM](https://wiki.openstreetmap.org/wiki/JOSM)
    * [JOSM Scripting Plugin](https://wiki.openstreetmap.org/wiki/JOSM/Plugins/Scripting)
* Select the correct layer, and run the appropriate script according to the Workflow section of the UtahBuildingsImport Wiki.
    * The scripts work on the currently active layer, so be sure the correct layer is selected.

### Contribution guidelines ###

Contributions are absolutely welcome! Please submit pull requests and I will review them, discuss the changes with you, and merge the changes.

### Who do I talk to? ###

Feel free to contact me, Jay Herron, with any questions at NeedleInAJayStack at protonmail.com
