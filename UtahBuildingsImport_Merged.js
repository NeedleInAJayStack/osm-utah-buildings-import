(function() { // the self-executing anonymous function
  var command = require("josm/command");
  var console = require("josm/scriptingconsole");

  // This contains all the different values of the PtType tag on the address points.
  // While the address points could reference things that are not buildings, it is assumed
  // that we have removed those points.
  // Only specify the tags if we can be more specific than building=yes
  const ptTypeMap = {"Agricultural":{"building":"farm_auxiliary"},
    "BASE ADDRESS":null,
    "Business":{"building":"commercial"},
    "Commercial":{"building":"commercial"},
    "commercial":{"building":"commercial"},
    "COMMERCIAL":{"building":"commercial"},
    "Education":{"building":"school"},
    "Government":{"building":"government"},
    "Industrial":{"building":"industrial"},
    "Med":{"building":"hospital"},
    "Mixed Use":null,
    "OTH":null,
    "Other":null,
    "OTHER":null,
    "Residential":{"building":"residential"},
    "RESIDENTIAL":{"building":"residential"},
    "Unknown":null,
    "Vacant":null}


  var layer = josm.layers.activeLayer; // Set to the active layer.
  var ds = layer.data;

  counter = 0;
  ds.each(function(obj){
    // Don't override building tag if already set.
    // If building type wasn't set by building data or by hand in merge, use "PtType" tag
    if(obj.get("building")=="yes" && obj.has("PtType")){
      var tags = ptTypeMap[obj.get("PtType")]; // Find the ptType entry
      if(tags != null) {
        Object.keys(tags).forEach(function(key){ // Set each tag defined in the ptType result.
          obj.set(key, tags[key]);
        });
        counter = counter + 1;
      }
    }

    // Assign utahagrc:parcelid if it's not set and can be. This should only occur on address points without buildings.
    if(obj.get("utahagrc:parcelid")==null && obj.get("ParcelID")!=null) obj.set("utahagrc:parcelid",obj.get("ParcelID"));
  });
  josm.alert("Adjusted building tagging on "+counter+" objects.");
})();
